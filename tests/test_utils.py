# first line, do not remove
from unittest import TestCase
from io import StringIO

from utilities import generic_file, read_csv, DataBlock, RequiredParamMissing


class TestGenericFile(TestCase):

    def test_generic_file_like(self):
        file = StringIO('1\n2\n\n')
        with generic_file(file) as f:
            self.assertEqual(next(f).strip(), '1')
            self.assertEqual(next(f).strip(), '2')
            self.assertEqual(next(f).strip(), '')
            self.assertRaises(StopIteration, next, f)
        with generic_file(file) as f:
            self.assertEqual(next(f).strip(), '1')
            self.assertEqual(next(f).strip(), '2')
            self.assertEqual(next(f).strip(), '')
            self.assertRaises(StopIteration, next, f)

    def test_generic_file_true(self):
        file = __file__
        with generic_file(file) as f:
            self.assertEqual(next(f).strip(), '# first line, do not remove')
        with generic_file(file) as f:
            self.assertEqual(next(f).strip(), '# first line, do not remove')


class TestReadCsv(TestCase):

    def test_default(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertEqual(list(read_csv(file)), [['A', 'B', 'C'], ['D', 'E', 'F']])

    def test_skip(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertEqual(list(read_csv(file, skip=1)), [['D', 'E', 'F']])

    def test_limit(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertEqual(list(read_csv(file, limit=1)), [['A', 'B', 'C']])

    def test_sep(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertEqual(list(read_csv(file, sep=None)), ['A;B;C', 'D;E;F'])

    def test_no_strip(self):
        file = StringIO("\nA;B ;C\n\nD;E;F  \n\n")
        self.assertEqual(list(read_csv(file)), [['A', 'B ', 'C'], ['D', 'E', 'F']])

    def test_strip(self):
        file = StringIO("\nA;B ;C\n\nD;E;F  \n\n")
        self.assertEqual(list(read_csv(file, strip=True)), [['A', 'B', 'C'], ['D', 'E', 'F']])

    def test_filter(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertEqual(list(read_csv(file, filter=(0, str.startswith, 'A'))), [['A', 'B', 'C']])


class TestDataBlock(TestCase):

    def test_basic(self):
        data = DataBlock(a=1, b=2)
        self.assertEqual(data.a, 1)
        self.assertEqual(data.b, 2)
        data.update(b=3, c=4)
        self.assertEqual(data.a, 1)
        self.assertEqual(data.b, 3)
        self.assertEqual(data.c, 4)

    def test_factory_nominal(self):
        DataB = DataBlock.factory('DataB', required=('a', 'b'), defaults={'b': 5})
        data = DataB(a=1)
        self.assertEqual(data.a, 1)
        self.assertEqual(data.b, 5)
        data = DataB(a=1, b=2)
        self.assertEqual(data.a, 1)
        self.assertEqual(data.b, 2)

    def test_factory_missing(self):
        DataB = DataBlock.factory('DataB', required=('a',))
        self.assertRaises(RequiredParamMissing, DataB)
