from unittest import TestCase
from io import StringIO

from exceptions import *
from read_csv import CSVRecord, CSVIterator, CSVReader
from utilities import DataBlock


class TestCSVRecord(TestCase):

    def test_constructor(self):
        header = ['A', 'B', 'C']
        record = ['D', 'E', 'F']
        cr = CSVRecord(header, record, 'toto')
        self.assertEqual(cr.list_header, header)
        self.assertEqual(cr.record, record)
        self.assertEqual(cr.file_csv, 'toto')

    def test_equality(self):
        header = ['A', 'B', 'C']
        record = ['D', 'E', 'F']
        cr1 = CSVRecord(header, record, 'toto')
        cr2 = CSVRecord(header, record, 'titi')
        self.assertEqual(cr1, cr2)

    def test_getitem(self):
        header = ['A', 'B', 'C']
        record = ['D', 'E', 'F']
        cr = CSVRecord(header, record, 'toto')
        self.assertEqual(cr['A'], 'D')
        self.assertRaises(KeyError, CSVRecord.__getitem__, cr, 'X')

    def test_str(self):
        header = ['A', 'B', 'C']
        record = ['D', 'E', 'F']
        cr = CSVRecord(header, record, 'toto')
        self.assertEqual(str(cr), "CSVRecord(('A', 'D'), ('B', 'E'), ('C', 'F')...) from toto")


class TestCSVIterator(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.DataBlock = DataBlock.factory('DataB', required=('list_header', 'file_csv'),
                    defaults=dict(sep=';', skip=1, limit=None, filter=None, raw=True, check=True))

    def test_constructor(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        header = ['A', 'B', 'C']
        data = self.DataBlock(list_header=header, file_csv=file)
        ci = CSVIterator(data)
        self.assertEqual(ci.params.list_header, header)
        self.assertEqual(ci.params.file_csv, file)

    def test_iter_raw(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        header = ['A', 'B', 'C']
        data = self.DataBlock(list_header=header, file_csv=file)
        ci = CSVIterator(data)
        self.assertEqual(next(ci), ['D', 'E', 'F'])
        self.assertRaises(StopIteration, next, ci)

    def test_iter(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        header = ['A', 'B', 'C']
        data = self.DataBlock(list_header=header, file_csv=file, raw=False, skip=0)
        ci = CSVIterator(data)
        self.assertIsInstance(next(ci), CSVRecord)
        self.assertEqual(len(list(ci)), 1)

    def test_check(self):
        file = StringIO("\nA;B;C\n\nD;E;F;G\n\n")
        header = ['A', 'B', 'C']
        data = self.DataBlock(list_header=header, file_csv=file)
        ci = CSVIterator(data)
        self.assertRaises(ReadCSVMismatch, next, ci)

    def test_str(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        header = ['A', 'B', 'C']
        data = self.DataBlock(list_header=header, file_csv=file)
        ci = CSVIterator(data)
        self.assertEqual(str(ci)[:35], "CSVIterator(<_io.StringIO object at")


class TestCSVReader(TestCase):

    def test_constructor(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        cr = CSVReader(file)
        self.assertEqual(cr.params.file_csv, file)
        self.assertEqual(cr.params.list_header, ['A', 'B', 'C'])

    def test_empty_file(self):
        file = StringIO("\n\n\n")
        self.assertRaises(ReadCSVEmptyFile, CSVReader, file)

    def test_bad_sep(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        self.assertRaises(ReadCSVBadSeparator, CSVReader, file, sep=',')

    def test_iter(self):
        file = StringIO("A;B;C")
        cr = CSVReader(file, raw=True, check=False)
        it = iter(cr)
        self.assertIsInstance(it, CSVIterator)
        self.assertTrue(it.params.raw)
        self.assertFalse(it.params.check)

    def test_len(self):
        file = StringIO("\nA;B;C\n\nD;E;F\n\n")
        cr = CSVReader(file)
        self.assertEqual(len(cr), 1)
