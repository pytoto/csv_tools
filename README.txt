Python 3
A set of classes and tools to automate CSV reading, cleaning, filtering, sorting and writing

Launch tests:

cd tests
PYTHONPATH='../app' python3 -m unittest
