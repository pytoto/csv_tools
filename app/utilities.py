from contextlib import contextmanager


@contextmanager
def generic_file(file):
    """ context manager facilitating unittests on objects using files
    :param file: a file name (real case) or a file like object (test case)
    WARNING: File like object is not copied so it is not possible to use
    2 identical file like objects at the same time
    """
    if isinstance(file, str):
        file = open(file, 'r')
        yield file
        file.close()
    else:
        file.seek(0)
        yield file


def read_csv(file_path, sep=';', skip=0, limit=None, filter=None, strip=False):
    """ Generator that reads lines from a (CSV) file
    :param file_path: string
    :param sep: string, field separator, will not split if None
    :param skip: integer>=0, skip this first lines (usually to skip header)
    :param limit: integer>0, limit the number of lines read from file (default is no limit)
    :param filter: (index>=0, function(2), value), optionally filters the records from the table,
        requires that a sep is specified.
        e.g. filter=(3, str.startswith, '45') will filter on 4th field starting with '45'
    :param strip: if True, strip ws of each field
    """
    with generic_file(file_path) as f:
        if filter:
            filter_indice, filter_func, filter_value = filter
        for line in f:
            line = line.strip()  # skip empty lines
            if line:
                if skip:
                    skip -= 1
                    continue
                if limit:
                    limit -= 1
                elif limit is 0:
                    break
                if sep:
                    line = line.split(sep)
                if not filter or filter_func(line[filter_indice], filter_value):
                    yield [d.strip() for d in line] if strip and sep else line


class RequiredParamMissing(Exception):
    pass


class DataClass(object):
    """ class that shares data between objects
    """
    def __init__(self, **kwargs):
        if hasattr(self, '_defaults'):
            self.__dict__.update(self._defaults)
        self.__dict__.update(**kwargs)
        if hasattr(self, '_required'):
            for att in self._required:
                if not hasattr(self, att):
                    raise RequiredParamMissing(att)

    def update(self, **kwargs):
        self.__dict__.update(**kwargs)

    @classmethod
    def factory(cls, name, required, defaults=None):
        return type(name, (cls,), dict(_required=required, _defaults=defaults or {}))
