
class ReadCSVEmptyFile(Exception):
    pass


class ReadCSVBadSeparator(Exception):
    pass


class ReadCSVMismatch(Exception):
    pass
