
from exceptions import *
from utilities import generic_file, read_csv, DataClass


class CSVReader(object):

    def __init__(self, file_csv, sep=';', skip=0, limit=None, filter=None,
                 raw=False, check=True):
        """ CSVReader is an iterable over a CSV file
        :param file_csv: a file  name or a file object
        :param sep: a single char separator
        :param skip: number of data records to skip
        :param limit: number of data records to iterate over, no limit if None
        :param filter: a triple (index>=0, function(2), value), optionally filters
               the records from the table, see utilities.read_csv
        :param raw: if True, render raw data lists, else render CSVRecord objects
        :param check: if True, check header length against each record length
        """
        self.params = DataClass(file_csv=file_csv,
                                sep=sep,
                                skip=skip + 1,  # always skip header when reading data
                                limit=limit,
                                filter=filter,
                                raw=raw,
                                check=check)
        # read first non blank line and set headers
        list_header = None
        with generic_file(file_csv) as f:
            for v in f:
                v = v.strip()
                if v:
                    list_header = v.split(sep)
                    break
        if not list_header:
            raise ReadCSVEmptyFile(file_csv)
        if len(list_header) <= 1:
            raise ReadCSVBadSeparator(file_csv, sep)
        self.params.list_header = list_header

    def __iter__(self):
        return CSVIterator(self.params)

    def __len__(self):
        p = self.params
        return sum(1 for l in read_csv(p.file_csv, None, p.skip, p.limit, p.filter))

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, self.params.file_csv)


class CSVIterator(object):

    def __init__(self, params):
        self.params = params
        self.len_list_header = len(self.params.list_header)
        self.it = iter(read_csv(params.file_csv, params.sep, params.skip, params.limit, params.filter))

    def __iter__(self):
        return self

    def __next__(self):
        elt = next(self.it)
        if self.params.check and len(elt) != self.len_list_header:
            raise ReadCSVMismatch("len(header)={} len(data)={}".format(self.len_list_header, len(elt)))
        if self.params.raw:
            return elt
        else:
            return CSVRecord(self.params.list_header, elt, self.params.file_csv)

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, self.params.file_csv)


class CSVRecord(object):

    def __init__(self, list_header, record, file_csv):
        self.list_header = list_header
        self.record = record
        self.file_csv = file_csv

    def __eq__(self, other):
        return self.record == other.record and self.list_header == other.list_header

    def __getitem__(self, item):
        try:
            return self.record[self.list_header.index(item)]
        except (ValueError, IndexError):
            raise KeyError("{} not found in record from {}".format(item, self.file_csv))

    def __str__(self):
        l = min(3, len(self.list_header))
        return "{}({}, {}, {}...) from {}".format(self.__class__.__name__,
                                                  *zip(self.list_header[:l], self.record),
                                                  self.file_csv)
